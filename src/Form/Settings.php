<?php

namespace Drupal\zendeskloader\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\tabsapi\Service\Common;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Settings extends ConfigFormBase
{
    /**
     * Constructs a \Drupal\system\ConfigFormBase object.
     *
     * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
     *   The factory for configuration objects.
     */
    public function __construct(
        ConfigFactoryInterface $config_factory
    ) {
        $this->setConfigFactory($config_factory);
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('config.factory')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'zendeskloader_settings_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $config = $this->config('zendeskloader.settings');

        $form['settings'] = [
            '#type' => 'fieldset',
            '#title' => $this->t('Zendesk settings'),
            'key' => [
                '#type' => 'textfield',
                '#title' => $this->t('Zendesk key'),
                '#default_value' => $config->get('key'),
                '#placeholder' => 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx',
                '#required' => true,
            ],
        ];

        $form['html'] = [
            '#type' => 'fieldset',
            '#title' => $this->t('Zendesk HTML'),
            'description' => [
                '#type' => '#markup',
                '#prefix' => '<p>',
                '#suffix' => '</p>',
                '#markup' => $this->t('So that the fake button looks just your existing Zendesk one, we will attempt to copy the Zendesk HTML from your configured button.')
            ],
            'instructions' => [
                '#type' => '#markup',
                '#prefix' => '<p>',
                '#suffix' => '</p>',
                '#markup' => $this->t('Once you have entered your Zendesk key above, click the button below to load the widget and copy the styling for the fake button. This will take a few seconds and you will see the Zendesk button appear and disappear.')
            ],
            'errors' => [
                '#type' => '#markup',
                '#prefix' => '<p>',
                '#suffix' => '</p>',
                '#markup' => $this->t('Should nothing appear to happen or an error pops up your key is probably wrong, reload the page and try again. If you don\'t reload there may be issues caused by remanents of the failed attempt remaining.')
            ],
            'container' => [
                '#type' => 'textarea',
                '#title' => $this->t('Zendesk iFrame'),
                '#default_value' => $config->get('container'),
                '#placeholder' => 'Fill in your Zendesk key above and click the button below.',
                '#required' => true,
            ],
            'content' => [
                '#type' => 'textarea',
                '#title' => $this->t('Zendesk Button'),
                '#default_value' => $config->get('content'),
                '#placeholder' => 'Fill in your Zendesk key above and click the button below.',
                '#required' => true,
            ],
            'comments' => [
                '#type' => 'checkbox',
                '#title' => $this->t('Remove comments from the CSS'),
                '#description' => $this->t('This will remove the comments from the CSS when the configuration is saved. Please be aware this will remove any licensing information contained within the comments.'),
                '#default_value' => $config->get('comments'),
                '#required' => false,
            ],
            'grab' => [
                '#type' => 'button',
                '#value' => $this->t('Get code from Zendesk'),
                '#states' => [
                    'disabled' => [
                        ':input[name="key"]' => ['value' => ''],
                    ],
                ],
                '#attributes' => [
                    'onclick' => 'return false;'
                ],
                '#attached' => [
                    'library' => [
                        'zendeskloader/zendeskloader_settings'
                    ],
                ],
            ],
        ];

        $form['tweaks'] = [
            '#type' => 'fieldset',
            '#title' => $this->t('Zendesk tweaks'),
            'zindex' => [
                '#type' => 'textfield',
                '#title' => $this->t('Button z-index'),
                '#description' => 'Change the z-index of the button, i.e. if you want to hide it behind something else.',
                '#default_value' => $config->get('zindex'),
                '#placeholder' => '999998',
                '#required' => false,
            ],
        ];

        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        parent::submitForm($form, $form_state);

        // remove the comments from the CSS if requireds
        $content = $form_state->getValue('content');
        if (!empty($form_state->getValue('comments'))) {
            $regex = array(
                "`^([\t\s]+)`ism"=>'',
                "`^\/\*(.+?)\*\/`ism"=>"",
                "`(\A|[\n;]+)/\*.+?\*/`s"=>"$1",
                "`(\A|[;\s]+)//.+\R`"=>"$1\n",
                "`(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+`ism"=>"\n"
                );

            $content = preg_replace(array_keys($regex), $regex, $content);
        }

        $this->config('zendeskloader.settings')->set(
            'key',
            $form_state->getValue('key')
        )->set(
            'container',
            $form_state->getValue('container')
        )->set(
            'content',
            $content
        )->set(
            'comments',
            $form_state->getValue('comments')
        )->set(
            'zindex',
            $form_state->getValue('zindex')
        )->save();
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames()
    {
        return array(
            'zendeskloader.settings'
        );
    }
}
