<?php

namespace Drupal\zendeskloader\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * @Block(
 *   id = "zendeskloader_block",
 *   admin_label = @Translation("Zendesk Loader Widget"),
 * )
 */
class ZendeskLoader extends BlockBase implements ContainerFactoryPluginInterface
{
    /**
     * {@inheritdoc}
     */
    public function __construct(
        array $configuration,
        $plugin_id,
        $plugin_definition
    ) {
        parent::__construct($configuration, $plugin_id, $plugin_definition);
    }


    /**
     * {@inheritdoc}
     */
    public static function create(
        ContainerInterface $container,
        array $configuration,
        $plugin_id,
        $plugin_definition
    ) {
        return new static(
            $configuration,
            $plugin_id,
            $plugin_definition
        );
    }

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        $show = true;
        $config = \Drupal::config('zendeskloader.settings');

        // check settings before attempting to load
        if (empty($config->get('key'))) {
            drupal_set_message(t('Zendesk key is missing from your configuration.'), 'warning');
            $show = false;
        }
        if (empty($config->get('container'))) {
            drupal_set_message(t('Zendesk module needs to download your widget.'), 'warning');
            $show = false;
        }
        if (empty($config->get('content'))) {
            drupal_set_message(t('Zendesk module needs to download your widget.'), 'warning');
            $show = false;
        }

        if ($show) {
            $tweaks = [
                'zindex' => $config->get('zindex')
            ];

            $drupalSettings = [
                'key' => $config->get('key'),
                'container' => str_replace('id="launcher"', 'id="zendeskloader"', $config->get('container')),
                'content' => $config->get('content'),
                'tweaks' => $tweaks,
            ];

            return [
                '#attached' => [
                    'library' => [
                        'zendeskloader/zendeskloader',
                    ],
                    'drupalSettings' => [
                        'zendeskloader' => $drupalSettings,
                    ],
                ],
            ];
        }

        return [];
    }
}