(function ($) {
  function loadZendeskChat(key, callback) {
    var zdscript = document.createElement('script');
    zdscript.setAttribute('id', 'ze-snippet');
    zdscript.src = 'https://static.zdassets.com/ekr/snippet.js?key=' + key;
    document.getElementsByTagName('body')[0].appendChild(zdscript);

    // set timeout to catch the widget not loading
    window.zdcatch = setTimeout(function () {
      clearInterval(window.zdonload);
      callback();
    }, 5000);
    window.zdonload = setInterval(
      function () {
        if (typeof zE !== 'undefined' && typeof zE.activate !== 'undefined') {
          clearTimeout(window.zdcatch);
          clearInterval(window.zdonload);
          callback();
        }
      },
      50,
      null
    );
  }

  function clearLocalStorage() {
    var arr = []; // Array to hold the keys
    // Iterate over localStorage and insert the keys that meet the condition into arr
    for (var i = 0; i < localStorage.length; i++) {
      if (localStorage.key(i).substring(0, 3) == 'ZD-') {
        arr.push(localStorage.key(i));
      }
    }

    // Iterate over arr and remove the items by key
    for (var i = 0; i < arr.length; i++) {
      localStorage.removeItem(arr[i]);
    }
  }

  Drupal.behaviors.zendesk_grab = {
    attach: function (context) {
      var btn = document.getElementById('edit-grab');
      btn.addEventListener(
        'click',
        function () {
          var key = document.getElementById('edit-key').value;
          if (
            !/^[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}/.test(
              key
            ) ||
            key.length != 36
          ) {
            alert("The entered Zendesk key doesn't match the required format.");
            return;
          }

          document.getElementById('edit-grab').value = 'Please wait...';

          clearLocalStorage();

          loadZendeskChat(key, function () {
            // wait for a few seconds to get the displayed version of the iframe
            setTimeout(function () {
              if (document.getElementById('launcher')) {
                // grab iframe
                var container = document.getElementById('launcher').outerHTML;
                document.getElementById('edit-container').value = container;
                var content = new XMLSerializer().serializeToString(
                  document
                    .getElementById('launcher')
                    .contentWindow.document.getElementsByTagName('body')[0]
                );
                document.getElementById('edit-content').value = content;

                document
                  .getElementById('launcher')
                  .contentWindow.document.getElementsByTagName('body')[0]
                  .remove();
              } else {
                alert(
                  'Sorry, something went wrong loading the Zendesk widget - please recheck your key.'
                );
                document.getElementById('edit-key').focus();
              }

              document.getElementById('edit-grab').value =
                'Get code from Zendesk';
            }, 3000);
          });
        },
        false
      );
    },
  };
})();
