(function ($) {
  function showButton() {
    if (
      document
        .getElementById('zendeskloader')
        .contentWindow.document.getElementsByTagName('button').length > 0
    ) {
      var iframe = document.getElementById('zendeskloader');
      iframe.style.opacity = 1;
    }
  }

  function hideButton() {
    var iframe = document.getElementById('zendeskloader');
    if (iframe) {
      iframe.style.opacity = 0;
    }
  }

  function loadZendeskChat(callback) {
    var zdscript = document.createElement('script');
    zdscript.setAttribute('id', 'ze-snippet');
    zdscript.src =
      'https://static.zdassets.com/ekr/snippet.js?key=' +
      drupalSettings.zendeskloader.key;
    document.getElementsByTagName('body')[0].appendChild(zdscript);

    window.zdonload = setInterval(
      function () {
        if (typeof zE !== 'undefined' && typeof zE.activate !== 'undefined') {
          clearInterval(window.zdonload);

          // tweak the button if required
          if (parseInt(drupalSettings.zendeskloader.tweaks.zindex) > 0) {
            var launcher = document.getElementById('launcher');
            if (launcher) {
              launcher.style.zIndex =
                drupalSettings.zendeskloader.tweaks.zindex;
            }
          }

          callback();
        }
      },
      50,
      null
    );
  }

  window.loadAndOpenZendeskChat = function () {
    // if already loaded, just activate
    if (typeof zE !== 'undefined' && typeof zE.activate !== 'undefined') {
      zE.activate();
    } else {
      // load in the Zendesk JS
      loadZendeskChat(function () {
        localStorage.setItem('zendeskloader_hasOpened', true);

        // sort out buttons, temporarily hide the zendesk one or it'll appear and disappear (and still does in Firefox)
        var launcher = document.getElementById('launcher');
        if (launcher) {
          launcher.style.display = 'none';
        }
        hideButton();

        window.setTimeout(function () {
          zE.activate();
          // unhide the zendesk one as we've hidden ours
          setTimeout(function () {
            var launcher = document.getElementById('launcher');
            if (launcher) {
              launcher.style.display = 'block';
            }
          }, 1000);
        }, 1000);
      });
    }
  };

  // set up the button when its found in the DOM
  function setupButton() {
    // set up a timeout, if the button isn't ready within a second try and create it again
    // for some reason in Firefox it sometimes doesn't add it
    window.zdWatch = setTimeout(function () {
      clearInterval(window.zdSetup);
      createButton();
    }, 1000);

    window.zdSetup = setInterval(function () {
      if (
        document
          .getElementById('zendeskloader')
          .contentWindow.document.getElementsByTagName('button').length > 0
      ) {
        clearTimeout(window.zdWatch);
        clearInterval(window.zdSetup);

        document
          .getElementById('zendeskloader')
          .contentWindow.document.getElementsByTagName('button')[0]
          .addEventListener('click', function () {
            loadAndOpenZendeskChat();
          });

        showButton();
      }
    }, 50);
  }

  // add the button in the iframe
  function createButton() {
    window.zdButton = setInterval(function () {
      if (document.getElementById('zendeskloader')) {
        clearInterval(window.zdButton);

        document.getElementById(
          'zendeskloader'
        ).contentWindow.document.documentElement.innerHTML =
          drupalSettings.zendeskloader.content;

        setupButton();
      }
    }, 50);
  }

  // set up our own iframe for the ZD button
  function createContainer() {
    var iframe = new DOMParser()
      .parseFromString(drupalSettings.zendeskloader.container, 'text/html')
      .getElementById('zendeskloader');
    iframe.style.opacity = 0;
    // apply tweaks
    if (parseInt(drupalSettings.zendeskloader.tweaks.zindex) > 0) {
      iframe.style.zIndex = drupalSettings.zendeskloader.tweaks.zindex;
    }
    document.getElementsByTagName('body')[0].appendChild(iframe);

    createButton();
  }

  Drupal.behaviors.zendesk_loader = {
    attach: function (context) {
      // start loading the ZenDesk JS is the widget has already been opened
      $(document).once('zendeskloader_behavior').each( function() {
        if (localStorage.getItem('zendeskloader_hasOpened')) {
          loadZendeskChat(function () {});
        } else {
          createContainer();
        }
      });
    },
  };
})(jQuery);
